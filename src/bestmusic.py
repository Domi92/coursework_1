import ConfigParser

from flask import Flask, render_template, redirect, url_for

app = Flask(__name__)

@app.route('/')
def root():
  return render_template('home.html')

@app.route('/artists/<genre>/')
def artists(genre):
  template = (genre + '.html')
  try:
    return render_template(template)
  except:
    return redirect(url_for('artists', genre = 'all'))

@app.route('/artists/electro/<artist>/')
@app.route('/artists/rock/<artist>/')
@app.route('/artists/classical/<artist>/')
@app.route('/artists/all/<artist>/')
def showartist(artist):
  template = (artist + '.html')
  try:
    return render_template(template)
  except:
    return redirect(url_for('artists', genre = 'all'))

@app.route('/albums/')
def albums():
  return render_template('albums.html')

@app.route('/tracks/')
def tracks():
  return render_template('tracks.html')

@app.errorhandler(404)
def page_not_found(error):
  return render_template('error.html')

def init(app):
  config = ConfigParser.ConfigParser()
  try:
    config_location = "etc/defaults.cfg"
    config.read(config_location)

    app.config['DEBUG'] = config.get("config", "debug")
    app.config['ip_address'] = config.get("config", "ip_address")
    app.config['port'] = config.get("config", "port")
    app.config['url'] = config.get("config", "url")
  except:
    print "Could not read config from: ", config_location

if __name__ == '__main__':
  init(app)
  app.run(
    host = app.config['ip_address'],
    port = int(app.config['port']))
