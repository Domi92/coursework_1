To see the web-app "bestmusic" follow the following instructions:

1. Open the livenux environment
2. On Windows open a ssh client like putty, on linux or mac you don't need a
ssh client. Just open the command line.
3. ssh into livenux.
    Host Name: tc@localhost
    password: foo
4. Navigate to coursework_1/src/
5. Type: python bestmusic.py
6. Open your favorite web browser and type "localhost:5000" into your address
line.
7. Enjoy the web app

